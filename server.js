import express from "express";
import { DataTypes, Sequelize } from "sequelize";
import { config } from "dotenv";

config()

const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    {
        host: process.env.DB_HOST,
        dialect: "mysql"
    }
);

export const Sku = sequelize.define("SKU", {
    id: { type: DataTypes.INTEGER, unique: true, primaryKey: true, autoIncrement: true },
    sku: DataTypes.STRING,
    description: DataTypes.STRING,
    customerDescription: DataTypes.STRING,
    unitCost: DataTypes.DECIMAL,
    unitPrice: DataTypes.DECIMAL,
    hardCost: DataTypes.TINYINT,
    groupid: DataTypes.INTEGER,
}, {
    timestamps: true,
    createdAt: "dateadded",
    updatedAt: "dateupdated",
    freezeTableName: true,
});

await sequelize.authenticate();

const app = express();

app.use(express.json());

app.get("/db/sku", async (request, response) => {
    response.json(await Sku.findAll());
});

app.post("/db/sku/create", async (request, response) => {
    console.debug("CREATE", request.body);
    const model = await Sku.create(request.body);
    response.json(model.id);
});

app.patch("/db/sku/update", async (request, response) => {
    console.debug("UPDATE", request.query.id, request.body);
    await Sku.update(request.body, { where: { id: request.query.id } });
    response.json();
});

app.delete("/db/sku/delete", async (request, response) => {
    console.debug("DELETE", request.query.id);
    await Sku.destroy({ where: { id: request.query.id } });
    response.json();
});

app.listen(3001, () => {
    console.log("-".repeat(15), "Started Express server.", "-".repeat(15));
});