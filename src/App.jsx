import { Routes, Route } from "react-router-dom";
import { Box } from "@mui/material";
import Home from "./components/Home";
import Sidebar from "./components/Sidebar";
import Topbar from "./components/Topbar";
import OpenTickets from "./components/OpenTickets";
import ZabbixProblems from "./components/ZabbixProblems";
import SlaTicket from "./components/SlaTicket";
import NewTickets from "./components/NewTickets";
import SkuTable from "./components/SkuTable";

export default function App() {
  return (
    <Box display="flex" className="content">
      <Sidebar />
      <Box flexGrow="1">
        <Topbar />
        <Box p={4}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/skuTable" element={<SkuTable />} />
            <Route path="/openTickets" element={<OpenTickets />} />
            <Route path="/zabbixProblems" element={<ZabbixProblems isDashboard={false} />} />
            <Route path="/slaTickets" element={<SlaTicket isDashboard={false} />} />
            <Route path="/newTickets" element={<NewTickets isDashboard={false} />} />
          </Routes>
        </Box>
      </Box>
    </Box>
  );
}
