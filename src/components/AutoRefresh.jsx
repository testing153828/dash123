import { useEffect } from "react";

export default function AutoRefresh({ interval, onRefresh }) {
  useEffect(() => {
    const timer = setInterval(() => {
      onRefresh();
    }, interval);

    return () => {
      clearInterval(timer);
    };
  }, [interval, onRefresh]);

  return null;
}
