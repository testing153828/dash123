import { useState, useEffect } from "react";
import {
  DataGrid, GridActionsCellItem, GridRowModes,
  GridToolbarColumnsButton, GridToolbarContainer, GridToolbarDensitySelector,
  GridToolbarFilterButton, GridRowEditStopReasons, useGridApiContext
} from '@mui/x-data-grid';
import { Box, Typography, Button, useTheme } from '@mui/material';
import CheckIcon from "@mui/icons-material/CheckOutlined";
import CloseIcon from "@mui/icons-material/CloseOutlined";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import EditIcon from "@mui/icons-material/EditOutlined";
import AddIcon from "@mui/icons-material/AddOutlined";

function EditToolbar({ rows, setRows, setRowsModesModel }) {
  const apiRef = useGridApiContext();
  const clickAdd = () => {
    setRows([...rows, {
      id: 0,
      sku: "",
      description: "",
      customerDescription: "",
      unitCost: 0.00,
      unitPrice: 0.80,
      hardCost: 0,
      groupid: "",
      actions: "",
      dateadded: new Date(),
      dateupdated: new Date(),
      new: true
    }]);
    setRowsModesModel((oldModel) => ({
      ...oldModel, 0: { mode: GridRowModes.Edit },
    }));
    apiRef.current.setPage(0);
  };
  return (
    <GridToolbarContainer>
      <Button startIcon={<AddIcon />} onClick={clickAdd}>Create</Button>
      <GridToolbarColumnsButton />
      <GridToolbarFilterButton />
      <GridToolbarDensitySelector />
    </GridToolbarContainer>
  );
}


function SkuTable() {
  const theme = useTheme();
  const [rows, setRows] = useState([]);
  const [rowsModesModel, setRowsModesModel] = useState({});
  const handleRowEditStop = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };
  const clickSave = (id) => () => {
    setRowsModesModel({ ...rowsModesModel, [id]: { mode: GridRowModes.View } });
  };
  const clickCancel = (id) => () => {
    setRowsModesModel({ ...rowsModesModel, [id]: { mode: GridRowModes.View, ignoreModifications: true } });
    if (!id) {
      setRows(rows.filter((row) => (row.id !== id)));
    }
  };
  const clickEdit = (id) => () => {
    setRowsModesModel({ ...rowsModesModel, [id]: { mode: GridRowModes.Edit } });
  };
  const clickDelete = (id) => async () => {
    await fetch(`/db/sku/delete?id=${id}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" }
    }).then((response) => {
      setRows(rows.filter((row) => row.id !== id));
    });
  };
  const processRowUpdate = async (newRow) => {
    if (newRow.new) {
      const response = (await (await fetch(`/db/sku/create`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          sku: newRow.sku,
          description: newRow.description,
          groupid: newRow.groupid,
          customerDescription: newRow.customerDescription,
          hardCost: Number(newRow.hardCost),
          unitCost: Number.parseFloat(newRow.unitCost),
          unitPrice: Number.parseFloat(newRow.unitPrice)
        })
      })).json());
      const updatedRow = { ...newRow, new: false, id: response };
      setRows(rows.map((row) => (row.id === 0 ? updatedRow : row)));
    } else {
      await fetch(`/db/sku/update?id=${newRow.id}`, {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          sku: newRow.sku,
          description: newRow.description,
          groupid: newRow.groupid,
          customerDescription: newRow.customerDescription,
          hardCost: Number(newRow.hardCost),
          unitCost: Number.parseFloat(newRow.unitCost),
          unitPrice: Number.parseFloat(newRow.unitPrice)
        })
      });
    }
    return newRow;
  };
  const handleRowModesModelChange = (newRowModesModel, event) => {
    setRowsModesModel(newRowModesModel);
  };
  const columns = [
    {
      field: "id",
      headerName: "ID", type: "number", flex: 1,
      align: "left", headerAlign: "left"
    },
    {
      field: "sku",
      preProcessEditCellProps: ({ id, row, props, hasChanged }) => ({ ...props, error: !props.value }),
      headerName: "SKU", type: "string", flex: 3,
      align: "left", headerAlign: "left", editable: true
    },
    {
      field: "description",
      preProcessEditCellProps: ({ id, row, props, hasChanged }) => ({ ...props, error: !props.value }),
      headerName: "Description", type: "string", flex: 3,
      align: "left", headerAlign: "left", editable: true
    },
    {
      field: "customerDescription",
      headerName: "Description (Customer)", type: "string", flex: 3,
      align: "left", headerAlign: "left", editable: true
    },
    {
      field: "unitCost", valueFormatter: (value) => ("$" + value.value),
      preProcessEditCellProps: ({ id, row, props, hasChanged }) => ({ ...props, error: !props.value }),
      headerName: "Cost per Unit", type: "number", flex: 1,
      align: "left", headerAlign: "left", editable: true
    },
    {
      field: "unitPrice", valueFormatter: (value) => (value.value * 100 + "%"),
      preProcessEditCellProps: ({ id, row, props, hasChanged }) => ({ ...props, error: !props.value }),
      headerName: "Price Percentage", type: "number", flex: 1,
      align: "left", headerAlign: "left", editable: true
    },
    {
      field: "hardCost",
      headerName: "Hard Cost", type: "boolean", flex: 1,
      align: "left", headerAlign: "left", editable: true,
    },
    {
      field: "groupid",
      preProcessEditCellProps: ({ id, row, props, hasChanged }) => ({ ...props, error: !props.value }),
      headerName: "Group", type: "number", flex: 1,
      align: "left", headerAlign: "left", editable: true
    },
    {
      field: "dateadded", valueGetter: ({ value }) => new Date(Date.parse(value)),
      headerName: "Date Added", type: "date", flex: 2,
      align: "left", headerAlign: "left"
    },
    {
      field: "dateupdated", valueGetter: ({ value }) => new Date(Date.parse(value)),
      headerName: "Date Updated", type: "date", flex: 2,
      align: "left", headerAlign: "left"
    },
    {
      field: "actions",
      headerName: "Actions", type: "actions",
      align: "left", headerAlign: "left",
      getActions: ({ id }) => (
        rowsModesModel[id]?.mode === GridRowModes.Edit ? [
          <GridActionsCellItem icon={<CheckIcon />} label="Save" onClick={clickSave(id)} />,
          <GridActionsCellItem icon={<CloseIcon />} label="Cancel" onClick={clickCancel(id)} />
        ] : [
          <GridActionsCellItem icon={<EditIcon />} label="Edit" onClick={clickEdit(id)} />,
          <GridActionsCellItem icon={<DeleteIcon />} label="Delete" onClick={clickDelete(id)} />
        ]
      )
    }
  ];
  useEffect(() => {
    fetch("/db/sku")
      .then((response) => response.json())
      .then((rows) => setRows(rows));
  }, []);
  return (
    <Box height="75vh">
      <Typography variant="h1" gutterBottom>SKU Table</Typography>
      <Box height="100%">
        <DataGrid
          sx={{
            "& .MuiDataGrid-booleanCell[data-value=true]": {
              color: theme.palette.primary.main
            }
          }}
          initialState={{
            columns: {
              columnVisibilityModel: {
                dateadded: false,
              },
            },
            sorting: {
              sortModel: [{ field: "sku", sort: "asc" }],
            },
          }}
          density="compact"
          autoPageSize
          rows={rows}
          columns={columns}
          editMode="row"
          onRowEditStop={handleRowEditStop}
          rowModesModel={rowsModesModel}
          onRowModesModelChange={handleRowModesModelChange}
          processRowUpdate={processRowUpdate}
          onProcessRowUpdateError={(error) => { throw error; }}
          slots={{
            toolbar: EditToolbar,
          }}
          slotProps={{
            toolbar: { rows, setRows, setRowsModesModel }
          }}
        />
      </Box>
    </Box>
  );
}
export default SkuTable;