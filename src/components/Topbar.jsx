import { Box } from "@mui/material";
import { Link } from "react-router-dom";

export default function Topbar() {
  return (
    <Box px={4} py={2}>
      <Link to="/"><img src="logo_black.svg" alt="Choice Solutions Logo" height="50px" /></Link>
    </Box>
  );
}
