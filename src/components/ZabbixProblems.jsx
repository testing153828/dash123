import { useState, useEffect } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { Box, Typography } from "@mui/material";
import ContextMenu from "./ContextMenu";
import "../css/component/core.css";

export default function ZabbixProblems({ isDashboard }) {
  const [rows, setRows] = useState([]);
  const [selectedRowIds, setSelectedRowIds] = useState([]);
  const [contextMenuPosition, setContextMenuPosition] = useState(null);
  const [showTicketPopup, setShowTicketPopup] = useState(false);
  const [selectedTicketData, setSelectedTicketData] = useState(null);

  const handleOptionSelected = (option) => {
    if (option === "OpenTicket" && selectedTicketData) {
      setShowTicketPopup(true);
    } else {
      setContextMenuPosition(null);
      setShowTicketPopup(false);
    }
    console.log(option);
  };

  useEffect(() => {
    fetch("http://10.100.10.152:5000/zabbix/sql/problems")
      .then((response) => response.json())
      .then((data) => setRows(data));
  }, []);

  const columns = [
    {
      field: "eventid",
      headerName: "Event ID",
      width: 90,
      renderCell: (params) => (
        <a href={`https://zmonitoring.choicecloud.com/zabbix/tr_events.php?triggerid=${params.row.triggerid}&eventid=${params.row.eventid}`} target="_blank" rel="noopener noreferrer">
          {params.row.eventid}
        </a>
      )
    },
    {
      field: "triggerid",
      headerName: "Trigger ID",
      width: 90,
    },
    {
      field: "problem_name",
      headerName: "Summary",
      width: isDashboard ? 300 : 550,
    },
    {
      field: "Company_ID",
      headerName: "Company",
      width: isDashboard ? 150 : 200,
    },
    {
      field: "host_name",
      headerName: "Host Name",
      width: 175,
    },
    {
      field: "date_time",
      headerName: "Date Entered",
      width: isDashboard ? 140 : 150,
      valueGetter: (params) => (
        new Intl.DateTimeFormat("en-US", {
          dateStyle: "short",
          timeStyle: "short",
          timeZone: "America/Chicago",
        }).format(
          new Date(params.row.date_time)
        )
      )
    },
    {
      field: "priority",
      headerName: "Priority",
      width: 125,
    },
    {
      field: "acknowledged",
      headerName: "Acknowledged",
      width: 125,
    },
  ];

  const handleSelectionModelChange = (newSelection) => {
    const selectionModel = newSelection?.selectionModel || [];
    setSelectedRowIds(selectionModel);
  };

  const handleContextMenu = (event, rowParams) => {
    event.preventDefault();
    const clickedRowElement = event.target.closest(".MuiDataGrid-row");
    console.log(clickedRowElement);
    if (clickedRowElement) {
      const clickedRowId = clickedRowElement.getAttribute("data-id");
      const selectedData = rows.find(
        (row) => String(row?.eventid) === clickedRowId
      );

      if (selectedData) {
        setSelectedTicketData(selectedData);
      } else {
        console.log("Selected data not found.");
      }
    }

    setContextMenuPosition({ x: event.clientX, y: event.clientY });
  };

  return (
    <Box>
      <Typography variant="h1" gutterBottom>Zabbix Problems</Typography>
      <Box onContextMenu={handleContextMenu}>
        <DataGrid
          autoHeight
          rows={rows}
          disableSelectionOnClick
          columns={columns}
          classes={"ticketlist"}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 15,
              },
            },
            columns: {
              columnVisibilityModel: {
                triggerid: !isDashboard,
                host_name: !isDashboard,
                priority: !isDashboard,
                acknowledged: !isDashboard
              }
            }
          }}
          pageSizeOptions={[15, 30, 50, 100]}
          getRowId={(row) => row?.eventid}
          checkboxSelection
          onSelectionModelChange={handleSelectionModelChange}
        />
        {contextMenuPosition && (
          <ContextMenu
            position={contextMenuPosition}
            onOptionSelected={handleOptionSelected}
            selectedData={selectedTicketData}
          />
        )}
        {showTicketPopup && selectedTicketData && (
          <Box className="ticketPopup">
            <h2>Ticket Information</h2>
            <ul>
              <li>
                <strong>Event ID:</strong> {selectedTicketData.eventid}
              </li>
              <li>
                <strong>Trigger ID:</strong> {selectedTicketData.triggerid}
              </li>
              <li>
                <strong>Company ID:</strong> {selectedTicketData.Company_ID}
              </li>
              <li>
                <strong>Priority:</strong> {selectedTicketData.priority}
              </li>
              <li>
                <strong>Item Name:</strong> {selectedTicketData.item_name}
              </li>
            </ul>
            <button onClick={() => setShowTicketPopup(false)}>Submit</button>
          </Box>
        )}
      </Box>
    </Box>
  );
}
