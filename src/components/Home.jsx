import { Box, Grid } from "@mui/material";
import FeaturedInfo from "./FeaturedInfo";
import SlaTicket from "./SlaTicket";
import NewTickets from "./NewTickets";
import ZabbixProblems from "./ZabbixProblems";

export default function Home() {
  return (
    <Box flex="4">
      <Grid container spacing={4}>
        <Grid item xs={12}><FeaturedInfo /></Grid>
        <Grid item xs={12}><NewTickets isDashboard={true} /></Grid>
        <Grid item xs={6}><SlaTicket isDashboard={true} /></Grid>
        <Grid item xs={6}><ZabbixProblems isDashboard={true} /></Grid>
      </Grid>
    </Box>
  );
}
