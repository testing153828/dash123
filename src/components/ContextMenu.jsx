import { useEffect, useRef } from "react";
import { Box } from "@mui/material";

export default function ContextMenu({ position, onOptionSelected, selectedTableData }) {
  const menuRef = useRef(null);

  const handleOptionClick = (option) => {
    onOptionSelected(option, selectedTableData);
  };

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (menuRef.current && !menuRef.current.contains(event.target)) {
        onOptionSelected(null, selectedTableData);
      }
    };

    document.addEventListener("click", handleClickOutside);

    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, [onOptionSelected, selectedTableData]);

  return (
    <Box
      ref={menuRef}
      style={{
        position: "absolute",
        top: position.y,
        left: position.x,
        background: "#fff",
        border: "1px solid #ccc",
        zIndex: 1,
        borderRadius: "10px",
        padding: "5px 6px",
      }}
    >
      <ul
        style={{
          listStyleType: "none",
          fontSize: "18px",
          cursor: "pointer",
          margin: "2px",
          padding: "0 5px 0 10px",
          alignItems: "center",
        }}
      >
        <li onClick={() => handleOptionClick("Edit")}>Edit</li>
        <li onClick={() => handleOptionClick("Delete")}>Delete</li>
        <li onClick={() => handleOptionClick("Bundle")}>Bundle</li>
        <li onClick={() => handleOptionClick("OpenTicket")}>Open Ticket</li>
      </ul>
    </Box>
  );
};
