import { useState, useEffect } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { Box, Typography } from "@mui/material";
import ContextMenu from "./ContextMenu";
import "../css/component/core.css";

export default function SlaTicket({ isDashboard }) {
  const [rows, setRows] = useState([]);
  const [selectedTableData, setSelectedTableData] = useState([]);
  const [selectedRowIds, setSelectedRowIds] = useState([]);
  const [contextMenuPosition, setContextMenuPosition] = useState(null);

  const handleOptionSelected = (option) => {
    // Handle the selected option (e.g., edit, delete)
    console.log("Selected option:", option);
    setContextMenuPosition(null); // Close the context menu
  };

  useEffect(() => {
    fetch("http://10.100.10.152:5000/cwapi/tickets/sla")
      .then((response) => response.json())
      .then((response) => setRows(response));
  }, []);

  const columns = [
    {
      field: "id",
      headerName: "ID",
      width: 90,
      renderCell: (params) => (
        <a href={`https://portal.choicecloud.com/v4_6_release/services/system_io/Service/fv_sr100_request.rails?service_recid=${params.row.id}`} target="_blank" rel="noopener noreferrer">
          {params.row.id}
        </a>
      )
    },
    {
      field: "summary",
      headerName: "Summary",
      width: isDashboard ? 300 : 425,
    },
    {
      field: "company",
      headerName: "Company",
      width: isDashboard ? 135 : 150,
      valueGetter: (params) => {
        let result = [];
        if (params.row.company) {
          if (params.row.company.identifier) {
            result.push(params.row.company.identifier);
          } else {
            result = ["Unknown"];
          }
          return result.join(", ");
        }
      },
    },
    {
      field: "board",
      headerName: "Board",
      width: 125,
      valueGetter: (params) => {
        let result = [];
        if (params.row.board) {
          if (params.row.board.name) {
            result.push(params.row.board.name);
          } else {
            result = ["Unknown"];
          }
          return result.join(", ");
        }
      },
    },
    {
      field: "status",
      headerName: "Status",
      width: 150,
      valueGetter: (params) => {
        let result = [];
        if (params.row.status) {
          if (params.row.status.name) {
            result.push(params.row.status.name);
          } else {
            result = ["Unknown"];
          }
          return result.join(", ");
        }
      },
    },
    {
      field: "priority",
      headerName: "Priority",
      width: 125,
      valueGetter: (params) => {
        let result = [];
        if (params.row.priority) {
          if (params.row.priority.name) {
            result.push(params.row.priority.name);
          } else {
            result = ["Unknown"];
          }
          return result.join(", ");
        }
      },
    },
    {
      field: "source",
      headerName: "Source",
      width: 160,
      valueGetter: (params) => {
        let result = [];
        if (params.row.source) {
          if (params.row.source.name) {
            result.push(params.row.source.name);
          } else {
            result = ["Unknown"];
          }
          return result.join(", ");
        }
      },
    },
    {
      field: "_info",
      headerName: "Updated By",
      width: isDashboard ? 135 : 150,
      valueGetter: (params) => {
        let result = [];
        if (params.row._info) {
          if (params.row._info.updatedBy) {
            result.push(params.row._info.updatedBy);
          } else {
            result = ["Unknown"];
          }
          return result.join(", ");
        }
      },
    },
    {
      field: "owner",
      headerName: "Owner",
      width: isDashboard ? 135 : 150,
      valueGetter: (params) => {
        let result = [];
        if (params.row.owner) {
          if (params.row.owner.name) {
            result.push(params.row.owner.name);
          } else {
            result = ["Unknown"];
          }
          return result.join(", ");
        }
      },
    },
  ];

  const handleSelectionModelChange = (newSelection) => {
    setSelectedRowIds(newSelection.selectionModel || []); // Handle undefined selectionModel
    setSelectedTableData(
      (newSelection.selectionModel || []).map((selectedId) =>
        rows.find((row) => row.id === selectedId)
      )
    );
  };

  return (
    <Box>
      <Typography variant="h1" gutterBottom>SLA Violated</Typography>
      <Box
        onContextMenu={(e) => {
          e.preventDefault();
          setContextMenuPosition({ x: e.clientX, y: e.clientY });
        }}
      >
        <DataGrid
          autoHeight
          rows={rows}
          disableSelectionOnClick
          columns={columns}
          classes={"ticketlist"}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 15,
              },
            },
            columns: {
              columnVisibilityModel: {
                board: !isDashboard,
                status: !isDashboard,
                priority: !isDashboard,
                source: !isDashboard,
              }
            }
          }}
          pageSizeOptions={[15, 30, 50, 100]}
          getRowId={(row) => row.id}
          checkboxSelection
          onSelectionModelChange={handleSelectionModelChange}
        />
        {contextMenuPosition && (
          <ContextMenu
            position={contextMenuPosition}
            onOptionSelected={handleOptionSelected}
            selectedTableData={selectedTableData}
          />
        )}
      </Box>
    </Box>
  );
}
