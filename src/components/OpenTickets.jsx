import { useState, useEffect } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { Box, Typography } from "@mui/material";
import ContextMenu from "./ContextMenu";
import "../css/component/core.css";

export default function OpenTickets() {
  const [rows, setRows] = useState([]);
  const [selectedTableData, setSelectedTableData] = useState([]);
  const [selectedRowIds, setSelectedRowIds] = useState([]);
  const [contextMenuPosition, setContextMenuPosition] = useState(null);
  const [isBundleFormOpen, setIsBundleFormOpen] = useState(false);
  const [parentTicketNumber, setParentTicketNumber] = useState("");

  const handleOptionSelected = (option) => {
    // Handle the selected option (e.g., edit, delete, bundle)
    console.log("Selected option:", option);

    if (option === "Bundle") {
      setIsBundleFormOpen(true);
    }

    setContextMenuPosition(null); // Close the context menu
    console.log("Is bundle form open?", isBundleFormOpen);
  };

  const handleBundleSubmit = () => {
    // Handle bundle form submission
    console.log("Parent Ticket Number:", parentTicketNumber);

    // Perform the bundling operation with the selected tickets and parent ticket number

    // Clear the form and close it
    setParentTicketNumber("");
    setIsBundleFormOpen(false);
  };

  useEffect(() => {
    fetch("http://10.100.10.152:5000/cwapi/queries/cwOpenTickets")
      .then((response) => response.json())
      .then((response) => setRows(response));
  }, []);

  const columns = [
    {
      field: "SR_Service_RecID",
      headerName: "ID",
      width: 90,
      renderCell: (params) => (
        <a href={`https://portal.choicecloud.com/v4_6_release/services/system_io/Service/fv_sr100_request.rails?service_recid=${params.row.SR_Service_RecID}`} target="_blank" rel="noopener noreferrer">
          {params.row.SR_Service_RecID}
        </a>
      )
    },
    {
      field: "Summary",
      headerName: "Summary",
      width: 500,
    },
    {
      field: "Company_ID",
      headerName: "Company ID",
      width: 190,
    },
    {
      field: "Date_Entered_UTC",
      headerName: "Date Entered",
      width: 150,
      valueGetter: (params) => (
        new Intl.DateTimeFormat("en-US", {
          dateStyle: "short",
          timeStyle: "short",
          timeZone: "America/Chicago",
        }).format(
          new Date(params.row.Date_Entered_UTC)
        )
      )
    },
    {
      field: "Board_Name",
      headerName: "Board",
      width: 90,
    },
    {
      field: "Rec_Type",
      headerName: "Type",
      width: 90,
    },
    {
      field: "Status_Name",
      headerName: "Status",
      width: 200,
    },
    {
      field: "Contact_Full_Name",
      headerName: "Last Contact",
      width: 200,
    },
  ];

  const handleSelectionModelChange = (newSelection) => {
    setSelectedRowIds(newSelection.selectionModel || []); // Handle undefined selectionModel
    setSelectedTableData(
      (newSelection.selectionModel || []).map((selectedId) =>
        rows.find((row) => row.SR_Service_RecID === selectedId)
      )
    );
  };

  return (
    <Box>
      <Typography variant="h1" gutterBottom>Open Tickets</Typography>
      <Box
        onContextMenu={(event) => {
          event.preventDefault();
          setContextMenuPosition({ x: event.clientX, y: event.clientY });
        }}
      >
        <DataGrid
          autoHeight
          rows={rows}
          columns={columns}
          classes={"ticketlist"}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 15,
              },
            },
          }}
          pageSizeOptions={[15, 30, 50, 100]}
          getRowId={(row) => row.SR_Service_RecID}
          checkboxSelection
          onSelectionModelChange={handleSelectionModelChange}
        />
        {contextMenuPosition && (
          <ContextMenu
            position={contextMenuPosition}
            onOptionSelected={handleOptionSelected}
          />
        )}
        {isBundleFormOpen && (
          <Box className="bundleForm">
            <input
              type="text"
              value={parentTicketNumber}
              onChange={(e) => setParentTicketNumber(e.target.value)}
              placeholder="Enter Parent Ticket Number"
            />
            <button onClick={handleBundleSubmit}>Bundle</button>
          </Box>
        )}
      </Box>
    </Box>
  );
}
