import { useState } from 'react';
import { Link } from "react-router-dom";
import { Sidebar as ProSidebar, Menu, MenuItem, sidebarClasses } from 'react-pro-sidebar';
import AlarmIcon from "@mui/icons-material/Alarm";
import FiberNewSharpIcon from "@mui/icons-material/FiberNewSharp";
import HouseOutlinedIcon from "@mui/icons-material/HouseOutlined";
import LocalActivityIcon from "@mui/icons-material/LocalActivityOutlined";
import MenuIcon from "@mui/icons-material/MenuOutlined";
import ReportProblemIcon from "@mui/icons-material/ReportProblemOutlined";
import TagIcon from "@mui/icons-material/Tag";


export default function Sidebar() {
  const [isCollapsed, setIsCollapsed] = useState(true);
  return (
    <ProSidebar collapsed={isCollapsed} rootStyles={{
      [`.${sidebarClasses.container}`]: {
        backgroundColor: "#f5f5f5"
      },
    }}>
      <Menu>
        <MenuItem onClick={() => setIsCollapsed(!isCollapsed)}
          icon={<MenuIcon />}></MenuItem>
        <MenuItem icon={<HouseOutlinedIcon color="primary" />}
          component={<Link to="/" />}> Home</MenuItem>
        <MenuItem icon={<TagIcon color="primary" />}
          component={<Link to="/skuTable" />}> SKU Table</MenuItem>
        <MenuItem icon={<FiberNewSharpIcon color="primary" />}
          component={<Link to="/newTickets" />}> New Tickets</MenuItem>
        <MenuItem icon={<LocalActivityIcon color="primary" />}
          component={<Link to="/openTickets" />}> Open Tickets</MenuItem>
        <MenuItem icon={<AlarmIcon color="primary" />}
          component={<Link to="/slaTickets" />}> Sla Violated</MenuItem>
        <MenuItem icon={<ReportProblemIcon color="primary" />}
          component={<Link to="/zabbixProblems" />}> Zabbix Alerts</MenuItem>
      </Menu>
    </ProSidebar>
  );
}
