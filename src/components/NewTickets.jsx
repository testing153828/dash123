import { useState, useEffect } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { Box, Typography } from "@mui/material";
import ContextMenu from "./ContextMenu";
import "../css/component/core.css";

export default function NewTickets({ isDashboard }) {
  const [rows, setRows] = useState([]);
  const [selectedTableData, setSelectedTableData] = useState([]);
  const [selectedRowIds, setSelectedRowIds] = useState([]);
  const [contextMenuPosition, setContextMenuPosition] = useState(null);

  const handleOptionSelected = (option) => {
    // Handle the selected option (e.g., edit, delete)
    console.log("Selected option:", option);
    setContextMenuPosition(null); // Close the context menu
  };

  useEffect(() => {
    fetch("http://10.100.10.152:5000/cwapi/queries/cwNewTicket")
      .then((response) => response.json())
      .then((response) => setRows(response));
  }, []);

  const columns = [
    {
      field: "Rec_ID",
      headerName: "ID",
      width: 90,
      renderCell: (params) => (
        <a href={`https://portal.choicecloud.com/v4_6_release/services/system_io/Service/fv_sr100_request.rails?service_recid=${params.row.Rec_ID}`} target="_blank" rel="noopener noreferrer">
          {params.row.Rec_ID}
        </a>
      )
    },
    {
      field: "Summary",
      headerName: "Summary",
      width: 700,
    },
    {
      field: "Board Name",
      headerName: "Board",
      width: 200,
    },
    {
      field: "Type",
      headerName: "Type",
      width: 100,
    },
    {
      field: "Status Description",
      headerName: "Status",
      width: 150,
    },
    {
      field: "Company Name",
      headerName: "Company",
      width: 290,
    },
  ];

  const handleSelectionModelChange = (newSelection) => {
    setSelectedRowIds(newSelection.selectionModel || []); // Handle undefined selectionModel
    setSelectedTableData(
      (newSelection.selectionModel || []).map((selectedId) =>
        rows.find((row) => row.id === selectedId)
      )
    );
  };

  return (
    <Box>
      <Typography variant="h1" gutterBottom>New Tickets</Typography>
      <Box
        onContextMenu={(event) => {
          event.preventDefault();
          setContextMenuPosition({ x: event.clientX, y: event.clientY });
        }}
      >
        <DataGrid
          autoHeight
          rows={rows}
          columns={columns}
          classes={"ticketlist"}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: isDashboard ? 4 : 15,
              },
            },
          }}
          pageSizeOptions={isDashboard ? [4, 8, 16, 25] : [15, 30, 50, 100]}
          getRowId={(row) => row.Rec_ID}
          checkboxSelection
          onSelectionModelChange={handleSelectionModelChange}
        />
        {contextMenuPosition && (
          <ContextMenu
            position={contextMenuPosition}
            onOptionSelected={handleOptionSelected}
            selectedTableData={selectedTableData}
          />
        )}
      </Box>
    </Box>
  );
}