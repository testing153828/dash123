import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { CssBaseline, ThemeProvider, createTheme } from "@mui/material";
import App from "./App";
import "./index.css";


const theme = createTheme({
  spacing: 8,
  palette: {
    primary: {
      main: "#bb1924",
    },
    secondary: {
      main: "#888888",
    },
  },
  typography: {
    h1: {
      fontSize: "2rem"
    },
    h2: {
      fontSize: "1.8rem"
    },
    h3: {
      fontSize: "1.6rem"
    },
    h4: {
      fontSize: "1.4rem"
    },
    h5: {
      fontSize: "1.2rem"
    },
    h6: {
      fontSize: "0.8rem"
    },
  },
  shape: {
    borderRadius: 8,
  },
});

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <App />
      </ThemeProvider>
    </BrowserRouter>
  </React.StrictMode>
);
